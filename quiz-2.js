const ModalSecondQuiz = ({ handleClose, show, content }) => {
  const showClassname = show ? "modal display-block" : "modal display-none";
  return (
    <div className={showClassname}>
      <section className="modal-main">
        {content}
        <button onClick={handleClose}>close</button>
      </section>
    </div>
  );
};