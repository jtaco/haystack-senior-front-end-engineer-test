function firstQuiz(){
    let evenCursor = 0, oddCursor = values.length - 1, evenToChange = false, oddToChange = false;
    while (evenCursor < oddCursor && (evenCursor > values.length || oddCursor > 0)){
        evenToChange = evenToChange || values[evenCursor] % 2 !== 0;
        oddToChange = oddToChange || values[oddCursor] % 2 === 0;
        if (evenToChange && oddToChange) {
            [values[evenCursor], values[oddCursor]] = [values[oddCursor], values[evenCursor]];
            evenToChange = oddToChange = false;
        }
        if (!evenToChange) evenCursor++;
        if (!oddToChange) oddCursor--;
    }
}